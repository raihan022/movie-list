//
//  ViewController.swift
//  Movie List
//
//  Created by Raihan's Macbook Pro on 5/6/21.
//

import UIKit

class ViewController: UIViewController {
    
    // MARK: All variable declaration
    
    let movieListTableView = UITableView()
    
    var movieListModel = [AllMovieList]()
    var movieItemModel = [Result]()
    
    // MARK: ViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.getMoveInfoFromAPI()
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: SetupViews
    
    private func setupTableView()
    {
        movieListTableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(movieListTableView)
        
        movieListTableView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        movieListTableView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        movieListTableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        movieListTableView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        movieListTableView.showsVerticalScrollIndicator = false
        movieListTableView.delegate = self
        movieListTableView.dataSource = self
     
        movieListTableView.register(UINib(nibName: "MovieListTableViewCell", bundle: nil),forCellReuseIdentifier: "MovieListTableViewCell")
        
   
       
     
        
    }
    
    // MARK: API Components
    
    private func getMoveInfoFromAPI()
    {
        
        
        
        let url = mainUrl
        
        print(url)
        var request = URLRequest(url: URL(string: url)!)
        
        print("REQUEST: ",request)
        request.httpMethod = "GET"
        
        // request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request, completionHandler: { [self]  responsedata, response, error -> Void in
            
            guard let data = responsedata else {
                return
            }
            
            do {
                
                
                let allData = try JSONDecoder().decode(AllMovieList.self, from: data)
                
                
               
                if let data = allData.results{
                    movieItemModel = data
                }
               
                
                DispatchQueue.main.async {
                   
                    self.movieListTableView.reloadData()
                }
                
                
            } catch let error {
                print(" Error = ", error.localizedDescription)
            }
            
            
            
            
        })
        
        
        task.resume()
        
    }
    
    func checkModel()
    {
        
    }
}


extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return movieItemModel.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = movieListTableView.dequeueReusableCell(withIdentifier: "MovieListTableViewCell") as! MovieListTableViewCell
        
        if let titel = movieItemModel[indexPath.row].title{
            cell.movieTitleLabel.text = titel
        }
        if let overView = movieItemModel[indexPath.row].overview{
            cell.movieOverViewTextView.text = overView
        }
        
        
        if let str = movieItemModel[indexPath.row].posterPath{
            
            cell.photoActivityIndicator.startAnimating()
            cell.moviePosterImageView.downloadFrom(link: str, cell: cell)
        }
        else{
            cell.moviePosterImageView.image = UIImage(named: "MissingImage")
        }

        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 184
    }
    
    

}







extension UIImageView {
    func downloadFrom(link:String?, cell : MovieListTableViewCell?) {
        
        print(link)
        //link = "w2PMyoyLU22YvrGK3smVM9fW1jj.jpg"
        
        self.contentMode = .scaleAspectFill
        let str = "w2PMyoyLU22YvrGK3smVM9fW1jj.jpg"
        
        let myUrl = URL(string: "https://image.tmdb.org/t/p/w500\(link ?? "")")!
        
        image = UIImage(named: "default")
        if link != nil, let url = NSURL(string: str) {
            
            URLSession.shared.dataTask(with: myUrl as URL) { data, response, error in
                guard let data = data, error == nil else {
                    
                   
                    
                    return
                }
                
              
                
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                    print("statusCode != 200; \(httpResponse.statusCode)")
                    
                    return
                }
                DispatchQueue.main.async {
                    //print("\ndownload completed \(url.lastPathComponent!)")
                    
                    self.image = UIImage(data: data)
                    cell!.photoActivityIndicator.stopAnimating()
                    
                    
                }
                }.resume()
        } else {
            self.image = UIImage(named: "default")
            
        }
    }
}
