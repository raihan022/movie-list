//
//  MovieListTableViewCell.swift
//  Movie List
//
//  Created by Raihan's Macbook Pro on 5/6/21.
//

import UIKit

class MovieListTableViewCell: UITableViewCell {

    
    @IBOutlet weak var moviePosterImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieOverViewTextView: UITextView!
    
    var photoActivityIndicator : UIActivityIndicatorView = {
        
        let activity = UIActivityIndicatorView()
        
        activity.translatesAutoresizingMaskIntoConstraints = false
        
        
        
        return activity
        
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupConstraint()
        self.addActivity()
        
        
        
    }
    
    func setupConstraint()
    {
        moviePosterImageView.translatesAutoresizingMaskIntoConstraints = false
        moviePosterImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 30)
            .isActive = true
        movieOverViewTextView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        movieOverViewTextView.widthAnchor.constraint(equalToConstant: 114).isActive = true
        movieOverViewTextView.translatesAutoresizingMaskIntoConstraints = false
        movieOverViewTextView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 140).isActive = true
        moviePosterImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        movieOverViewTextView.topAnchor.constraint(equalTo: movieTitleLabel.bottomAnchor, constant: 5).isActive = true
        movieOverViewTextView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
//        
        
    }
    
    private func addActivity(){
        
        if #available(iOS 13, *){
            
            self.photoActivityIndicator.style = .medium
            
        }else{
            
            self.photoActivityIndicator.style = .white
        }
        
        
        contentView.addSubview(photoActivityIndicator)

        photoActivityIndicator.centerXAnchor.constraint(equalTo: moviePosterImageView.centerXAnchor).isActive = true
        photoActivityIndicator.centerYAnchor.constraint(equalTo: moviePosterImageView.centerYAnchor).isActive = true
    }


   
    
}
